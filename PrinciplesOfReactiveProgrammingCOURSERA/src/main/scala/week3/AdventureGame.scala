package week3

/**
 * Created by raitis on 12/08/15.
 */

// from T => S to T => try { S }
class Coin
class Treasure

trait Adventure {
  def collectCoins: List[Coin]
  def buyTreasure(coins: List[Coin]): Treasure
}


class AdventureGame extends Adventure {

  def collectCoins: List[Coin] = ???

  def buyTreasure(coins: List[Coin]): Treasure = ???



}

object Run extends App {

  val adventureGame = new AdventureGame()
//  val coins = adventureGame.collectCoins()
//  val treasure = adventureGame.buyTreasure(coins)
//
// Future Handles Exceptions and Latency


  println("ok")



}
