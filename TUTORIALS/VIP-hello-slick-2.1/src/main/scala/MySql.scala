import scala.slick.driver.MySQLDriver.simple._

/**
 * Created by raitis on 30/07/15.
 */


object MySql extends App {

  class Users(tag: Tag) extends Table[(Int, String, String)](tag, "USERS") {
    def id: Column[Int] = column[Int]("ID", O.PrimaryKey) // O.AutoInc
    def name: Column[String] = column[String]("NAME")
    def password: Column[String] = column[String]("PASSWORD")
    def * = (id, name, password)
  }

  class Authorities(tag: Tag) extends Table[(Int, Int, String)](tag, "AUTHORITIES") {
    def id = column[Int]("ID", O.PrimaryKey)
    def IDuser = column[Int]("ID_USER")
    def authority = column[String]("AUTHORITY")
    def * = (id, IDuser, authority)

    def user = foreignKey("AUTH_FK", IDuser, TableQuery[Users])(_.id)
  }



  val users: TableQuery[Users] = TableQuery[Users]
  val authorities = TableQuery[Authorities]

  val db = Database.forURL("jdbc:mysql://localhost/slick_intro", driver="com.mysql.jdbc.Driver", user="root", password ="")

  db.withSession { implicit session =>

    // delete
    (users.ddl ++ authorities.ddl).drop

    // create
    (users.ddl ++ authorities.ddl).create

    // insert
    users += (1, "Ray", "123")
    users += (2, "Bil", "345")

    // insert many
    val insertMeny = users ++= Seq (
      (3, "Bob", "567"),
      (4, "Sue", "789"),
      (5, "Clif", "999")
    )
    val insertAuthorities = authorities ++= Seq (
      (1, 4, "admin"),
      (2, 4, "user"),
      (3, 5, "user")
    )

    // number of inserted rows
    insertMeny foreach (n => println("added " + n + " rows"))

    // all users
    println(users.list map (x => x._2))

    // generated SQL
    println(users.selectStatement)

    // output, print each row (must be listed all columns)
    users foreach {
         case(id, name, password) => println(name)
    }

    // filter
    val filterQ = users filter (x => (x.id > 2))
    println(filterQ.selectStatement)
    filterQ foreach (x => println(x._2))

    // update
    val updateQ = users filter (x => (x.id === 3)) map (_.name)
    println(updateQ.selectStatement)
    val nr = updateQ.update("Rui")
    println("Updated " + nr + " rows")

    // delete
    val deleteQ = users filter (x=> (x.id === 1))
    println(deleteQ.selectStatement)
    val del = deleteQ.delete
    println("Deleted " + del + " rows")

    // sorting
    val sortQ = users sortBy(_.name)
    println(sortQ.list)

    // composition (take select number of rows)
    val compQ = users sortBy(_.id)  filter(_.id > 3) map (_.name) take(1)
    println(compQ.list)

    // join
    val joinQ = for {
      a <- authorities if a.authority === "user"
      u <- a.user
    } yield (a.authority, u.name)
    println("joined tables:" + joinQ.list)

    // agrigate functions
    val maxQ = users.map(_.password).max
    // println(maxQ run) didnt work

    // plain sql
    // required:
    import scala.slick.jdbc.StaticQuery.interpolation
    val id = "2"
    val plainQ = sql"SELECT * FROM `USERS` WHERE id=$id".as[String]
    println(plainQ.getStatement)
    // probably not the best practise
    println(users filter (x => (x.id === plainQ.list.head.toInt)) list)



  }


  println("ok")

}
